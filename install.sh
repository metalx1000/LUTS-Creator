#!/bin/bash
######################################################################
#Copyright (C) 2022  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
######################################################################

script_url="https://gitlab.com/metalx1000/LUTS-Creator/-/raw/master/src/lut_creator?inline=false"
script_file="/usr/local/bin/lut_creator"

if [ "$EUID" -ne 0 ]
then 
  echo "I restarting script as sudo."
  sudo $0
  exit
fi

apt update &&\
  apt install imagemagick gimp sxiv ffmpeg

wget "$script_url" -O "$script_file"
chmod +x "$script_file"
